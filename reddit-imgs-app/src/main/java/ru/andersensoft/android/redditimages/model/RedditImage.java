package ru.andersensoft.android.redditimages.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class RedditImage {

    @SerializedName("source")
    private RedditImageSource source;

    @SerializedName("id")
    private String id;

    @SerializedName("resolutions")
    private List<RedditImageSource> resolutions;

    public RedditImageSource getSource() {
        return source;
    }

    public void setSource(RedditImageSource source) {
        this.source = source;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<RedditImageSource> getResolutions() {
        return resolutions;
    }

    public void setResolutions(List<RedditImageSource> resolutions) {
        this.resolutions = resolutions;
    }
}
