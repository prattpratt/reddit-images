package ru.andersensoft.android.redditimages.model;

import com.google.gson.annotations.SerializedName;

public class RedditPostContainer {

    @SerializedName("kind")
    private String kind;

    @SerializedName("data")
    private RedditPost data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public RedditPost getData() {
        return data;
    }

    public void setData(RedditPost data) {
        this.data = data;
    }
}
