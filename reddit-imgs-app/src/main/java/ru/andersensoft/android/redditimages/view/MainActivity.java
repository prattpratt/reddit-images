package ru.andersensoft.android.redditimages.view;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import java.util.List;

import ru.andersensoft.android.redditimages.R;
import ru.andersensoft.android.redditimages.databinding.ActivityMainBinding;
import ru.andersensoft.android.redditimages.model.RedditPost;
import ru.andersensoft.android.redditimages.network.HttpClient;
import ru.andersensoft.android.redditimages.viewmodel.MainActivityViewModel;

public class MainActivity extends AppCompatActivity implements MainView {
    private MainActivityViewModel mViewModel;
    private ImageListAdapter mImageListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityMainBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        initRecyclerView(binding.recyclerView);
        mViewModel = new MainActivityViewModel(new HttpClient(), this);
        binding.setViewModel(mViewModel);
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        GridLayoutManager layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                // 5 is the sum of items in one repeated section
                switch (position % 5) {
                    // first item span 2 columns
                    case 0:
                        return 2;
                    // next 4 items span 1 columns each
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                        return 1;
                    default:
                        throw new IllegalStateException("Internal error");
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        mImageListAdapter = new ImageListAdapter();
        recyclerView.setAdapter(mImageListAdapter);
    }

    @Override
    public void onStart() {
        super.onStart();
        mViewModel.onStart();
    }

    @Override
    public void onShowMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onShowResults(List<RedditPost> posts) {
        mImageListAdapter.addItems(posts);
    }
}
