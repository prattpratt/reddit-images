package ru.andersensoft.android.redditimages.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class Preview {

    @SerializedName("images")
    private List<RedditImage> images;

    @SerializedName("enabled")
    private boolean enabled;

    public List<RedditImage> getImages() {
        return images;
    }

    public void setImages(List<RedditImage> images) {
        this.images = images;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
