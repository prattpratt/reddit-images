package ru.andersensoft.android.redditimages.view;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ru.andersensoft.android.redditimages.databinding.RedditImageItemBinding;
import ru.andersensoft.android.redditimages.model.RedditPost;

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.RedditPostVH> {

    private List<RedditPost> mItems = new ArrayList<>();

    @Override
    public RedditPostVH onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        return RedditPostVH.create(layoutInflater, parent);
    }

    @Override
    public void onBindViewHolder(RedditPostVH holder, int position) {
        holder.bindTo(mItems.get(position));
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void addItems(List<RedditPost> items) {
        if (getItemCount() > 0) {
            mItems.clear();
        }
        mItems.addAll(items);
        notifyDataSetChanged();
    }

    static class RedditPostVH extends RecyclerView.ViewHolder {

        private RedditImageItemBinding mBinding;

        private RedditPostVH(RedditImageItemBinding binding) {
            super(binding.getRoot());
            mBinding = binding;
        }

        static RedditPostVH create(LayoutInflater inflater, ViewGroup parent) {
            RedditImageItemBinding binding = RedditImageItemBinding.inflate(inflater, parent, false);
            return new RedditPostVH(binding);
        }

        void bindTo(RedditPost post) {
            mBinding.setPost(post);
            mBinding.executePendingBindings();
        }
    }
}
