package ru.andersensoft.android.redditimages.network;

import android.util.Log;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.andersensoft.android.redditimages.model.RedditPostsResult;

public class HttpClient {
    private static final String TAG = HttpClient.class.getSimpleName();
    private static final String BASE_URL = "https://www.reddit.com";
    private static final String ERROR_MSG = "Error while retrieving result from " + BASE_URL;

    private static final long DEFAULT_TIMEOUT_SECS = 10;
    private static final long UPLOAD_TIMEOUT_SECS = 60;

    private final ApiService mApiService;

    public HttpClient() {
        OkHttpClient httpClient = new OkHttpClient.Builder()
            .connectTimeout(DEFAULT_TIMEOUT_SECS, TimeUnit.SECONDS)
            .readTimeout(DEFAULT_TIMEOUT_SECS, TimeUnit.SECONDS)
            .writeTimeout(UPLOAD_TIMEOUT_SECS, TimeUnit.SECONDS)
            .build();

        Retrofit retrofit = new Retrofit.Builder()
            .client(httpClient)
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();

        mApiService = retrofit.create(ApiService.class);
    }

    public void getNewPosts(final GetResultListener listener) {
        Call<RedditPostsResult> call = mApiService.getNewPosts(100);
        getPosts(listener, call);
    }

    public void getTopPosts(final GetResultListener listener) {
        Call<RedditPostsResult> call = mApiService.getTopPosts(100);
        getPosts(listener, call);
    }

    private void getPosts(final GetResultListener listener, Call<RedditPostsResult> call) {
        call.enqueue(new Callback<RedditPostsResult>() {
            @Override
            public void onResponse(Call<RedditPostsResult> call,
                Response<RedditPostsResult> response) {

                if (response.isSuccessful()) {
                    RedditPostsResult result = response.body();
                    listener.onGetResultOk(result);
                } else {
                    String message = ERROR_MSG + '\n' + response;
                    Log.d(TAG, message);
                    listener.onGetResultFail(message);
                }
            }

            @Override
            public void onFailure(Call<RedditPostsResult> call, Throwable t) {
                Log.e(TAG, ERROR_MSG, t);
                listener.onGetResultFail(ERROR_MSG + '\n' + t);
            }
        });
    }

    public interface GetResultListener {
        void onGetResultOk(RedditPostsResult result);

        void onGetResultFail(String message);
    }
}
