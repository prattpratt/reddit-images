package ru.andersensoft.android.redditimages.viewmodel;

import android.databinding.ObservableBoolean;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.andersensoft.android.redditimages.model.PostHint;
import ru.andersensoft.android.redditimages.model.RedditPost;
import ru.andersensoft.android.redditimages.model.RedditPostContainer;
import ru.andersensoft.android.redditimages.model.RedditPostsResult;
import ru.andersensoft.android.redditimages.network.HttpClient;
import ru.andersensoft.android.redditimages.view.MainView;

public class MainActivityViewModel implements HttpClient.GetResultListener {
    private static final String TAG = MainActivityViewModel.class.getSimpleName();

    private HttpClient mHttpClient;
    private MainView mMainView;

    public final ObservableBoolean isLoading = new ObservableBoolean(false);

    public MainActivityViewModel(HttpClient httpClient, MainView mainView) {
        this.mHttpClient = httpClient;
        this.mMainView = mainView;
    }

    public void onStart() {
        onTopClick();
    }

    public void onNewClick() {
        switchToLoading();
        mHttpClient.getNewPosts(this);
    }

    public void onTopClick() {
        switchToLoading();
        mHttpClient.getTopPosts(this);
    }

    private void switchToLoaded() {
        isLoading.set(false);
    }

    private void switchToLoading() {
        isLoading.set(true);
    }

    @Override
    public void onGetResultOk(RedditPostsResult result) {
        switchToLoaded();
        List<RedditPostContainer> children = result.getData().getChildren();
        List<RedditPost> items = new ArrayList<>(children.size());
        for (RedditPostContainer child : children) {
            RedditPost post = child.getData();

            // use items only with "post_hint": "image"
            if (PostHint.IMAGE == post.getPostHint()) {
                items.add(post);
            }
        }
        mMainView.onShowResults(items);
    }

    @Override
    public void onGetResultFail(String message) {
        switchToLoaded();
        mMainView.onShowMessage(message);
        Log.e(TAG, message);
    }
}
