package ru.andersensoft.android.redditimages.viewmodel;

import android.databinding.BindingAdapter;
import android.graphics.drawable.AnimationDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;

import com.bumptech.glide.Glide;

import ru.andersensoft.android.redditimages.R;

public class BindingAdapters {

    @BindingAdapter("android:visibility")
    public static void setVisibility(View view, boolean visible) {
        view.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    @BindingAdapter("android:src")
    public static void setImageUrl(final ImageView view, String url) {
        Glide.with(view.getContext())
                .load(url.replace("&amp;", "&")) // fix broken URLs for smaller resolution images
                .placeholder(R.color.colorPlaceholder)
                .dontAnimate()
                .into(view);
    }
}
