package ru.andersensoft.android.redditimages.model;

import com.google.gson.annotations.SerializedName;

public class RedditPost {

    @SerializedName("thumbnail")
    private String thumbnail;

    @SerializedName("post_hint")
    private PostHint postHint;

    @SerializedName("preview")
    private Preview preview;

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public PostHint getPostHint() {
        return postHint;
    }

    public void setPostHint(PostHint postHint) {
        this.postHint = postHint;
    }

    public Preview getPreview() {
        return preview;
    }

    public void setPreview(Preview preview) {
        this.preview = preview;
    }
}
