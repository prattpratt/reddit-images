package ru.andersensoft.android.redditimages.model;

import com.google.gson.annotations.SerializedName;

public class RedditPostsResult {

    @SerializedName("kind")
    private String kind;

    @SerializedName("data")
    private DataContainer data;

    public String getKind() {
        return kind;
    }

    public void setKind(String kind) {
        this.kind = kind;
    }

    public DataContainer getData() {
        return data;
    }

    public void setData(DataContainer data) {
        this.data = data;
    }
}
