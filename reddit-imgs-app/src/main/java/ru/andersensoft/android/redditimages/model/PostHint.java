package ru.andersensoft.android.redditimages.model;

import com.google.gson.annotations.SerializedName;

public enum PostHint {
    @SerializedName("image") IMAGE,
    @SerializedName("link") LINK
}
