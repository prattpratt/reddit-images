package ru.andersensoft.android.redditimages.network;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.andersensoft.android.redditimages.model.RedditPostsResult;

public interface ApiService {

    @GET("/r/EarthPorn/top/.json")
    Call<RedditPostsResult> getTopPosts(@Query("limit") int limit);

    @GET("/r/EarthPorn/new/.json")
    Call<RedditPostsResult> getNewPosts(@Query("limit") int limit);
}
