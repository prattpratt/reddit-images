package ru.andersensoft.android.redditimages.view;

import java.util.List;

import ru.andersensoft.android.redditimages.model.RedditPost;

public interface MainView {

    void onShowMessage(String message);

    void onShowResults(List<RedditPost> posts);
}
