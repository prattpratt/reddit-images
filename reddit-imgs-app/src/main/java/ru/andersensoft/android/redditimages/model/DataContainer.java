package ru.andersensoft.android.redditimages.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class DataContainer {

    @SerializedName("modhash")
    private String modhash;

    @SerializedName("children")
    private List<RedditPostContainer> children;

    public String getModhash() {
        return modhash;
    }

    public void setModhash(String modhash) {
        this.modhash = modhash;
    }

    public List<RedditPostContainer> getChildren() {
        return children;
    }

    public void setChildren(List<RedditPostContainer> children) {
        this.children = children;
    }
}
